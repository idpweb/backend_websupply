from flask import Flask, request, Response, jsonify
import pika
import json
from prometheus_client import Counter, Gauge, Summary, Histogram, Info, start_http_server
from http import HTTPStatus
import db_comm
import time

app = Flask(__name__)

c = Counter('my_counter', 'This is my counter modified through /inc_counter')
g = Gauge('my_gauge',
          'This is my gauge modified through /inc_gauge, /dec_gauge, /set_gauge')
s = Summary('my_summary', 'This is my summary modified through /set_summary')
h = Histogram('my_histogram',
              'This is my histogram modified through /set_summary')
i = Info('my_info', 'This is my info')
i.info({'version': '1.0.1', 'buildhost': 'radu.ciobanu@upb.ro'})


@app.route("/inc_counter", methods=['POST'])
def inc_counter():
    c.inc()
    return "OK"


@app.route("/inc_gauge", methods=['POST'])
def inc_gauge():
    g.inc()
    return "OK"


@app.route("/dec_gauge", methods=['POST'])
def dec_gauge():
    g.dec()
    return "OK"


@app.route("/set_gauge", methods=['POST'])
def set_gauge():
    value = request.form["value"]
    g.set(float(value))
    return "OK"


@app.route("/set_summary", methods=['POST'])
def set_summary():
    value = request.form["value"]
    s.observe(float(value))
    return "OK"


@app.route("/set_histogram", methods=['POST'])
def set_histogram():
    value = request.form["value"]
    h.observe(float(value))
    return "OK"


@app.route("/send_mail", methods=['POST'])
def send_mail():
    payload = request.get_json(silent=True)

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()
    channel.queue_declare(queue='email_queue', durable=True)
    channel.basic_publish(
        exchange='',
        routing_key='email_queue',
        body=json.dumps(payload),
        properties=pika.BasicProperties(
            delivery_mode=2,
        ))
    connection.close()
    return "<p>OK</p>"

@app.route("/", methods=['GET'])
def hello_world():
    return "<p>Hello, World!</p>"




@app.route("/locations/add", methods=['POST'])
def add_location():
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "insert_location")

    return Response(status=HTTPStatus.CREATED)


@app.route("/resources/add", methods=['POST'])
def add_resource():
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "insert_resource")

    return Response(status=HTTPStatus.CREATED)


@app.route("/users/add", methods=['POST'])
def add_user():
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "insert_user")

    return Response(status=HTTPStatus.CREATED)


@app.route("/posts/add", methods=['POST'])
def add_post():
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "insert_post")

    return Response(status=HTTPStatus.CREATED)


@app.route("/locations/update/<int:id>", methods=["PUT"])
def update_location(id):
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "update_location")

    return Response(HTTPStatus.OK)


@app.route("/resources/update/<int:id>", methods=["PUT"])
def update_resource(id):
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "update_resource")

    return Response(HTTPStatus.OK)


@app.route("/users/update/<int:id>", methods=["PUT"])
def update_user(id):
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "update_user")

    return Response(HTTPStatus.OK)


@app.route("/posts/update/<int:id>", methods=["PUT"])
def update_post(id):
    payload = request.get_json(silent=True)
    db_comm.handle_db_write(payload, "update_post")

    return Response(HTTPStatus.OK)


@app.route("/locations", methods=['GET'])
def get_locations():
    return db_comm.handle_db_read("select_locations", (), "locations"), HTTPStatus.OK


@app.route("/resources", methods=['GET'])
def get_resources():
    return db_comm.handle_db_read("select_requests", (), "requests"), HTTPStatus.OK


@app.route("/users", methods=['GET'])
def get_users():
    return db_comm.handle_db_read("select_users", (), "users"), HTTPStatus.OK


@app.route("/posts", methods=['GET'])
def get_posts():
    return db_comm.handle_db_read("select_posts", (), "posts"), HTTPStatus.OK


@app.route("/users/username", methods=['GET'])
def get_user_by_username():
    payload = request.get_json(silent=True)
    if "username" in payload:
        return db_comm.handle_db_read("select_by_username", (payload["username"]), "users"), HTTPStatus.OK
    else:
        return HTTPStatus.BAD_REQUEST

if __name__ == "__main__":
    time.sleep(7)
    print("Backend started", flush=True)
    start_http_server(8000)

    db_comm.create_tables()

    db_comm.create_mock_data()

    app.run(host="0.0.0.0")
