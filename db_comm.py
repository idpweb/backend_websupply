import psycopg2
from flask import jsonify
import sql_commands
import mock_data

## DB communication ##

def convert_tuples_to_json(fields, tups):
    dict_list = []
    for tup in tups:
        d = {}
        for i, field in enumerate(fields):
            if field == "timestamp":
                d[field] = tup[i].strftime(sql_commands.DATE_FORMAT)
            else:
                d[field] = tup[i]
        dict_list.append(d)
    return jsonify(dict_list)


def handle_db_write(data_json, command, table):
    cmd_args = []
    try:
        for field in sql_commands.DB_STRUCTURE[table]:
            cmd_args.append(data_json[field])
    
        cmd_args = tuple(cmd_args)

        conn, cursor = connect()
        cursor.execute(sql_commands.SQL_COMMANDS[command], cmd_args)
        
        diconnect(cursor, conn)
    except Exception as e:
        print("Error in database access: {}".format(e), flush=True)
        

def handle_db_read(command, cmd_args, table):
    try:
        conn, cursor = connect()
        cursor.execute(sql_commands.SQL_COMMANDS[command], cmd_args)

        result = cursor.fetchall()

        diconnect(cursor, conn)
    except Exception as e:
        print("Error in database access: {}".format(e), flush=True)
    return convert_tuples_to_json(sql_commands.DB_STRUCTURE[table], result)


def connect():
    conn = None
    conn = psycopg2.connect(
        host="postgresql",
        database="websupply",
        user="student",
        password="student")
    cursor = conn.cursor()

    return conn, cursor

    
def diconnect(cursor, conn):
    if cursor is not None:
        cursor.close()
    
    if conn is not None:
        conn.commit()
        conn.close()


def create_tables():
    try:
        conn, cursor = connect()

        for command in sql_commands.CREATE_DB_COMMANDS:
            cursor.execute(command)
        
        diconnect(cursor, conn)
    except Exception as e:
        print("Error creating tables: {}".format(e), flush=True)


def create_mock_data():
    try:
        conn, cursor = connect()

        for command, entries in mock_data.MOCK_DATA.items():
            for entry in entries:
                cursor.execute(sql_commands.SQL_COMMANDS[command], entry)
        
        diconnect(cursor, conn)
    except Exception as e:
        print("Error creating mock data: {}, on entry: {}".format(e, entry), flush=True)