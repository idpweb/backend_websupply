MOCK_DATA = {
    "insert_location" : [
        ("Romania", "Bucharest"),
        ("Romania" , "Branceni"),
        ("Ukraine", "Mariupol"),
        ("Romania", "Vaslui")
    ],
    "insert_resource" : [
        ("Water", "Water"),
        ("Food", "Bread"),
        ("Food", "Eggs"),
        ("Clothes", "Cold")
    ],
    "insert_user" : [
        ("GigelMarcu", "gicu@gicu.com", "Gigel Marcu", "2022-10-13", "Refugee"),
        ("DaniMocanu", "dani@mocanu.com", "Dani Mocanu", "2022-10-13", "Refugee")
    ],
    "insert_post" : [
        ("1", "Need food in Vaslui",
            "Description", "true", "true", "1", "2", "20", "2022-10-13"),
        ("2", "Need food in Vaslui",
            "Description", "true", "true", "1", "2", "20", "2022-10-13")
    ]
}