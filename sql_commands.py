
DATE_FORMAT = "%Y-%m-%d"

DB_STRUCTURE = {
    "locations" : [ "id", "country", "city" ],
    "resources" : [ "id", "name", "description" ],
    "users" : [ "id", "username", "email", "fullname", "member_since", "description" ],
    "posts" : [ "id", "sender_id", "title", "description", "is_request", "is_active",
        "location_id", "resource_id", "quantity", "deadline" ]
}


SQL_COMMANDS = {
    "insert_location" :
    """
    INSERT INTO locations (country, city)
    VALUES (%s, %s) RETURNING id;
    """,

    "insert_resource" :
    """
    INSERT INTO resources (name, description)
    VALUES (%s, %s) RETURNING id;
    """,

    "insert_user" :
    """
    INSERT INTO users (username, email, fullname, member_since, description)
    VALUES (%s, %s, %s, %s, %s) RETURNING id;
    """,

    "insert_post" :
    """
    INSERT INTO posts (sender_id, title, description, is_request, is_active, location_id,
        resource_id, quantity, deadline)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, to_timestamp(%s, 'YYYY-MM-DD')) RETURNING id;
    """,

    "update_location" :
    """
    UPDATE locations
        SET country = %s,
            city = %s
        WHERE id = %s;
    """,

    "update_resource" :
    """
    UPDATE resources
        SET name = %s,
            description = %s
        WHERE id = %s;
    """,

    "update_user" :
    """
    UPDATE users
        SET username = %s,
            email = %s,
            fullname = %s,
            member_since = to_timestamp(%s, 'YYYY-MM-DD'),
            description = %s
        WHERE id = %s;
    """,

    "update_post" :
    """
    UPDATE posts
        SET sender_id = %s,
            title = %s,
            description = %s,
            is_request = %s,
            is_active = %s,
            location_id = %s,
            resource_id = %s,
            quantity = %s,
            deadline = to_timestamp(%s, 'YYYY-MM-DD')
        WHERE id = %s;
    """,

    "select_locations" :
    """
    SELECT * FROM locations;
    """,

    "select_resources" :
    """
    SELECT * FROM resources;
    """,

    "select_users" :
    """
    SELECT * FROM users;
    """,

    "select_posts" :
    """
    SELECT * FROM posts;
    """
}

SQL_QUERY = {
    "select_by_username" :
    """
    SELECT * FROM users
    WHERE username = %s;
    """,


}


CREATE_DB_COMMANDS = [
    """
    DROP TABLE IF EXISTS posts CASCADE;
    """,
    """
    DROP TABLE IF EXISTS users CASCADE;
    """,
    """
    DROP TABLE IF EXISTS resources CASCADE;
    """,
    """
    DROP TABLE IF EXISTS locations CASCADE;
    """,
    """
    CREATE TABLE locations (
        id SERIAL PRIMARY KEY,
        country VARCHAR(255),
        city VARCHAR(255)
    );
    """,
    """
    CREATE TABLE resources (
        id SERIAL PRIMARY KEY,
        name VARCHAR(255),
        description VARCHAR(255)
    );
    """,
    """
    CREATE TABLE users (
        id SERIAL PRIMARY KEY,
        username VARCHAR(255) UNIQUE,
        email VARCHAR(255) UNIQUE,
        fullname VARCHAR(255),
        member_since TIMESTAMP,
        description VARCHAR(1024)
    );
    """,
    """
    CREATE TABLE posts (
        id SERIAL PRIMARY KEY,
        sender_id INTEGER,
        title VARCHAR(255),
        description VARCHAR(1024),
        is_request BOOL,
        is_active BOOL,
        location_id INTEGER,
        resource_id INTEGER,
        quantity INTEGER,
        timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deadline TIMESTAMP,
        FOREIGN KEY (sender_id)
            REFERENCES users (id)
            ON DELETE CASCADE,
        FOREIGN KEY(location_id)
            REFERENCES locations (id)
            ON DELETE CASCADE,
        FOREIGN KEY(resource_id)
            REFERENCES resources (id)
            ON DELETE CASCADE
    );
    """
]